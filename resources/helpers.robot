***Settings***

Library         OperatingSystem

***Keywords***
Get Json
    [Arguments]     ${file_name}

    #Carrega o arquivo no formato json guarda em ${json_file} 
    ${json_file}         Get File        ${EXECDIR}/resources/fixtures/${file_name}

    #Transforma o json em dicionario qie é o formato padrão
    ${json_dict}=        Evaluate        json.loads($json_file)     json

    [return]        ${json_dict}